package lab2ex2.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author connor
 */
@Entity
public class Car30 extends Vehicle30 {

    @Basic
    private String make;
    @Basic
    private String model;
    @Basic
    private String year;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}