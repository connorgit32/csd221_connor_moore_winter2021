/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.controllers;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import lab5.controllers.exceptions.NonexistentEntityException;
import lab5.entities.PublicationJPA;

/**
 *
 * @author connor
 */
public class PublicationJPAJpaController implements Serializable {

    public PublicationJPAJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PublicationJPA publicationJPA) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(publicationJPA);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PublicationJPA publicationJPA) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            publicationJPA = em.merge(publicationJPA);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = publicationJPA.getId();
                if (findPublicationJPA(id) == null) {
                    throw new NonexistentEntityException("The publicationJPA with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PublicationJPA publicationJPA;
            try {
                publicationJPA = em.getReference(PublicationJPA.class, id);
                publicationJPA.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publicationJPA with id " + id + " no longer exists.", enfe);
            }
            em.remove(publicationJPA);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PublicationJPA> findPublicationJPAEntities() {
        return findPublicationJPAEntities(true, -1, -1);
    }

    public List<PublicationJPA> findPublicationJPAEntities(int maxResults, int firstResult) {
        return findPublicationJPAEntities(false, maxResults, firstResult);
    }

    private List<PublicationJPA> findPublicationJPAEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PublicationJPA as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PublicationJPA findPublicationJPA(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PublicationJPA.class, id);
        } finally {
            em.close();
        }
    }

    public int getPublicationJPACount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from PublicationJPA as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
