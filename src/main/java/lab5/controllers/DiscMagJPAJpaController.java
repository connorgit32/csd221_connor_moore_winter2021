/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.controllers;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import lab5.controllers.exceptions.NonexistentEntityException;
import lab5.entities.DiscMagJPA;

/**
 *
 * @author connor
 */
public class DiscMagJPAJpaController implements Serializable {

    public DiscMagJPAJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DiscMagJPA discMagJPA) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(discMagJPA);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DiscMagJPA discMagJPA) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            discMagJPA = em.merge(discMagJPA);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = discMagJPA.getId();
                if (findDiscMagJPA(id) == null) {
                    throw new NonexistentEntityException("The discMagJPA with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DiscMagJPA discMagJPA;
            try {
                discMagJPA = em.getReference(DiscMagJPA.class, id);
                discMagJPA.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The discMagJPA with id " + id + " no longer exists.", enfe);
            }
            em.remove(discMagJPA);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DiscMagJPA> findDiscMagJPAEntities() {
        return findDiscMagJPAEntities(true, -1, -1);
    }

    public List<DiscMagJPA> findDiscMagJPAEntities(int maxResults, int firstResult) {
        return findDiscMagJPAEntities(false, maxResults, firstResult);
    }

    private List<DiscMagJPA> findDiscMagJPAEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from DiscMagJPA as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DiscMagJPA findDiscMagJPA(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DiscMagJPA.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiscMagJPACount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from DiscMagJPA as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
