/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.controllers;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import lab5.controllers.exceptions.NonexistentEntityException;
import lab5.entities.MagazineJPA;

/**
 *
 * @author connor
 */
public class MagazineJPAJpaController implements Serializable {

    public MagazineJPAJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MagazineJPA magazineJPA) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(magazineJPA);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MagazineJPA magazineJPA) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            magazineJPA = em.merge(magazineJPA);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = magazineJPA.getId();
                if (findMagazineJPA(id) == null) {
                    throw new NonexistentEntityException("The magazineJPA with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MagazineJPA magazineJPA;
            try {
                magazineJPA = em.getReference(MagazineJPA.class, id);
                magazineJPA.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The magazineJPA with id " + id + " no longer exists.", enfe);
            }
            em.remove(magazineJPA);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MagazineJPA> findMagazineJPAEntities() {
        return findMagazineJPAEntities(true, -1, -1);
    }

    public List<MagazineJPA> findMagazineJPAEntities(int maxResults, int firstResult) {
        return findMagazineJPAEntities(false, maxResults, firstResult);
    }

    private List<MagazineJPA> findMagazineJPAEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from MagazineJPA as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MagazineJPA findMagazineJPA(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MagazineJPA.class, id);
        } finally {
            em.close();
        }
    }

    public int getMagazineJPACount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from MagazineJPA as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
