/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import lab5.entities.SaleableItem;



/**
 *
 * @author connor
 */
public class Ticket extends Publication implements SaleableItem {
    
    private String description;
    private double price;
    private String client;
    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }   
    
    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }
    
    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }
    
    public void sellCopy(int indexChoice) 
    {
        System.out.println("********************");
        System.out.println("   Ticket Voucher   ");
        System.out.println(toString());
        System.out.println("********************");
        System.out.println();     
    }
    
    
    
   
    @Override
    public String toString() {
        return    "Client: " + getClient() + "\n"
                + "Description: " + getDescription() + "\n"
                + "Price: " + getPrice() + "\n";      
    }
    
}
