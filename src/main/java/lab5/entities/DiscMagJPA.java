package lab5.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author connor
 */

@Entity
public class DiscMagJPA extends MagazineJPA {

    @Basic
    private boolean hasDisk;

    public boolean isHasDisk() {
        return hasDisk;
    }

    public void setHasDisk(boolean hasDisk) {
        this.hasDisk = hasDisk;
    }

}