/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.entities;



/**
 *
 * @author connor
 */
public interface SaleableItem {
    
    public void sellCopy(int indexChoice);
    public double getPrice();
    
}
