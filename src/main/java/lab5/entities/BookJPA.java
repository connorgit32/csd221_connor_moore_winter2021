package lab5.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author connor
 */

@Entity
public class BookJPA extends PublicationJPA {

    @Basic
    private String author;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

}