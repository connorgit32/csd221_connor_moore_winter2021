package lab5.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author connor
 */

@Entity
public class MagazineJPA extends PublicationJPA {

    @Basic
    private int orderQty;
    @Basic
    private int currIssue;

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    public int getCurrIssue() {
        return currIssue;
    }

    public void setCurrIssue(int currIssue) {
        this.currIssue = currIssue;
    }

}