/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;


import java.util.*;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import lab5.controllers.*;
import lab5.controllers.exceptions.NonexistentEntityException;
import lab5.entities.*;

/**
 *
 * @author connor
 */



public class App {

    // Declares a EMF, used for working with databases.
    private EntityManagerFactory emf;
    
    
    // If true, all code within run() will loop; if false end the program.
    public boolean loopBool = true;

    // Creates an object called 'input' from the class 'Scanner' which is a
    // part of the java.util library.
    final Scanner input = new Scanner(System.in);

    // List for each type of publication
    static List<Publication> pubList = new ArrayList<Publication>();
    
    // CashTill object that stores a running total
    CashTill ct = new CashTill();
    
    // List for each ticket
    static List<Ticket> ticketList = new ArrayList<Ticket>();
    
     // A menu to be displayed to the user.
    final String menu = ""
            + "1. Add Book\n"
            + "2. Add Magazine\n"
            + "3. Add Disk Magazine\n"
            + "4. Edit Publication\n"
            + "5. List Publication\n"
            + "6. Delete Publication\n"
            + "7. Sell Publication\n"
            + "8. Sell Ticket\n"
            + "9. Sell Pencil\n"
            + "10. Running total\n"
            + "99. quit";
    

    
    // To allow user to have spaces in input
        private String getInput(String s) {
        String ss = input.nextLine();
        if (ss.trim().isEmpty()) {
            return s;
        }
        Scanner in2 = new Scanner(ss);
        return in2.nextLine();
        }
        
        private int getInput(int i) {
        String s = input.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextInt();
        }
        
        private int getInput(double i) {
        String s = input.nextLine();
        if (s.trim().isEmpty()) {
            return (int) i;
        }
        Scanner in2 = new Scanner(s);
        return (int) in2.nextDouble();
        }

    
    
    public void run() throws Exception {

        while (loopBool == true) { // allows program to run in a loop, inputting 99 exits

            out.println(menu);

            int choice = 0;

            try { // This try-catch re-prompts the user for a menu selection if a non-integer is input.
                choice = input.nextInt();
            } catch (Exception e) {
                System.out.println("Please verify your selection was correct.");
                continue;
            }

            switch (choice) {
                case 1:
                    addBook();
                    break;
                case 2:
                    addMag();
                    break;
                case 3:
                    addDiskMag();
                    break;
                case 4:
                    editPub();
                    break;
                case 5:
                    listPub();
                    break;
                case 6:
                    deletePub();
                    break;
                case 7:
                    sellPub();
                    break;
                case 8:
                    sellTicket();
                    break;
                case 9:
                    sellPencil();
                    break;
                case 10:
                    runningTotal();
                    break;
                case 99:
                    loopBool = false;
                    break;
                default:
                    System.out.println("Bad input, try again please.");
            }
        }
    }
    
   
    public void addBook() {

        // Holds user input
        String stringChoice = null;
        int intChoice;
        double doubleChoice;
        
        // Book object which gets filled with data below, uses JPA
        BookJPA bookJPA = new BookJPA();
        // Is keeping List<Publication> only way to list books in ListPub()?
        Book book = new Book();
        
        // create EMF with persistent unit as parameter
        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        
        BookJPAJpaController bookJPAcontrol = new BookJPAJpaController(emf);
        
        // Inserts all needed attributes of Publication and Book objects.
        // Note: .next() doesn't allow spaces, nextLine() does.
        out.println("Title(press enter twice): ");
        stringChoice = input.nextLine();
        bookJPA.setTitle(getInput(stringChoice));
 
        //bookJPA.setTitle(getInput(stringChoice));
        book.setTitle(getInput(stringChoice));
        
        out.println("Copies: ");
        intChoice = input.nextInt();
        bookJPA.setCopies(getInput(intChoice));
        book.setCopies(intChoice);
        
        out.println("Price: ");
        doubleChoice = input.nextDouble();
        bookJPA.setPrice(doubleChoice);
        book.setPrice(doubleChoice);
        
        out.println("Author(press enter twice): ");
        stringChoice = input.nextLine();
        bookJPA.setAuthor(getInput(stringChoice));
        book.setAuthor(getInput(stringChoice));
             
        
        // Put in the pubList list array. 
        pubList.add(book);
        
        // Put book into the database
        bookJPAcontrol.create(bookJPA);
    }
    
    public void addMag() {

        // Holds user input
        String stringChoice;
        int intChoice;
        double doubleChoice;

        
        MagazineJPA magJPA = new MagazineJPA();
        Magazine mag = new Magazine();
        
        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        MagazineJPAJpaController magJPAcontrol = new MagazineJPAJpaController(emf);
        
        // Inserts all needed attributes of Publication and Magazine objects.
        out.println("Title(press enter twice): ");
        stringChoice = input.next();
        magJPA.setTitle(getInput(stringChoice));
        mag.setTitle(getInput(stringChoice));
        
        
        out.println("Copies: ");
        intChoice = input.nextInt();
        magJPA.setCopies(intChoice);
        mag.setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        magJPA.setPrice(doubleChoice);
        mag.setPrice(doubleChoice);

        out.println("Order Quantity: ");
        intChoice = input.nextInt();
        magJPA.setOrderQty(intChoice);
        mag.setOrderQty(intChoice);
        
        out.println("Current Issue: ");
        intChoice = input.nextInt();
        magJPA.setCurrIssue(intChoice);
        mag.setCurrIssue(intChoice);
        
        // Put in the pubList list array. 
        pubList.add(mag);

        magJPAcontrol.create(magJPA);
        
    }

    
    public void addDiskMag() {

        // Holds user input
        String stringChoice;
        int intChoice;
        double doubleChoice;

        
        DiscMagJPA dmagJPA = new DiscMagJPA();
        DiskMagazine dmag = new DiskMagazine();

        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        DiscMagJPAJpaController dmagJPAcontrol = new DiscMagJPAJpaController(emf);
        
        // Inserts all needed attributes of Publication and Magazine objects.
        out.println("Title(press enter twice): ");
        stringChoice = input.next();
        dmagJPA.setTitle(getInput(stringChoice));
        dmag.setTitle(getInput(stringChoice));
        
        out.println("Copies: ");
        intChoice = input.nextInt();
        dmagJPA.setCopies(intChoice);
        dmag.setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        dmag.setPrice(doubleChoice);
        dmag.setPrice(doubleChoice);

        out.println("Order Quantity: ");
        intChoice = input.nextInt();
        dmagJPA.setOrderQty(intChoice);
        dmag.setOrderQty(intChoice);
        
        out.println("Current Issue: ");
        intChoice = input.nextInt();
        dmagJPA.setCurrIssue(intChoice);
        dmag.setCurrIssue(intChoice);
        
        // Put in the pubList list array. 
        pubList.add(dmag);
        
        dmagJPAcontrol.create(dmagJPA);
    }
    
    
    
    
    
    
    private void editPub() throws Exception {

        int indexChoice; // stores pubList index
        int intChoice;
        String stringChoice;
        double doubleChoice;

        
        PublicationJPA pub = new PublicationJPA();

        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        PublicationJPAJpaController pubJPAcontrol = new PublicationJPAJpaController(emf);
        
        
        
        List<PublicationJPA> listPubs = pubJPAcontrol.findPublicationJPAEntities();
        
        
        out.println("Edit what publication?(index): ");
        intChoice = input.nextInt();
        
        
        out.println("Title(press enter twice): ");
        stringChoice = input.nextLine();
        listPubs.get(intChoice).setTitle(getInput(stringChoice));
                
        out.println("Copies: ");
        intChoice = input.nextInt();
        listPubs.get(intChoice).setCopies(getInput(intChoice));
        
        out.println("Price: ");
        doubleChoice = input.nextDouble();
        listPubs.get(intChoice).setPrice(doubleChoice);
        
        pubJPAcontrol.edit(listPubs.get(intChoice));
        

    }

    private void listPub() {
        // Lists all objects in list array using a for loop and the get() list array 
        // library method which uses indexes 
        //for (int i = 1; i <= pubList.size(); i++) { // i = 1, and <= to account for array syntax
            //System.out.println("Publication #" + i);      // making it more readable. First in array 
            //System.out.println(pubList.get(i - 1)); // will be object 1, not object 0.
        //}
        // .get works by utilizing the toString()
        // It can be overridden using @Override to customize what is displayed. 
        
        PublicationJPA pub = new PublicationJPA();

        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        PublicationJPAJpaController pubJPAcontrol = new PublicationJPAJpaController(emf);
        
        List<PublicationJPA> listPubs = pubJPAcontrol.findPublicationJPAEntities();
        
        for (PublicationJPA p: listPubs ) {
            out.println(p);
        }
        
    }

    
    private void deletePub() throws NonexistentEntityException {

        long intChoice;
        String stringChoice;
        double doubleChoice;
        
        PublicationJPA pub = new PublicationJPA();

        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        PublicationJPAJpaController pubJPAcontrol = new PublicationJPAJpaController(emf);

        
        List<PublicationJPA> listPubs = pubJPAcontrol.findPublicationJPAEntities();
        
        
        out.println("Edit what publication?(index): ");
        intChoice = input.nextLong();
     
        pubJPAcontrol.destroy(intChoice + 1);
        
        // Uses the Array List method remove() to delete people which handles
        // null spaces inbetween objects
        //pubList.remove(indexChoice - 1); // - 1 because we want people in the list
        // to start at 1, not 0 which is default for arrays.

    }

    private void sellPub() throws NonexistentEntityException {
        long longChoice; // choose a person from the personList list array

        PublicationJPA pub = new PublicationJPA();

        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        PublicationJPAJpaController pubJPAcontrol = new PublicationJPAJpaController(emf);

        
        List<PublicationJPA> listPubs = pubJPAcontrol.findPublicationJPAEntities();
        
        
        out.println("Which publication number would you like to sell?: ");
        longChoice = input.nextLong();
        int intChoice = (int)longChoice;
        
        pubJPAcontrol.destroy(longChoice + 1);
        
        
        
        // Lab 4 needed -1 for this to work 
        //ct.sellItem(pubList.get(indexChoice));
        ct.sellItem(pubList.get(intChoice));

       
        //pubList.get(indexChoice).sellCopy(indexChoice); 
        pubList.remove(intChoice - 1);
    }
    
    
    private void sellTicket() {
        // Holds user input
        String stringChoice;
        double doubleChoice;
        int indexChoice = 0;
        
        Ticket tkt = new Ticket();
        TicketJPA tktJPA = new TicketJPA();

        // Inserts all needed attributes of Publication and Magazine objects.
        out.println("Description: ");
        stringChoice = input.next();
        tkt.setDescription(getInput(stringChoice));
        tktJPA.setDescription(getInput(stringChoice));

        
        out.println("Price: ");
        doubleChoice = input.nextDouble();
        tkt.setPrice(getInput(doubleChoice));
        tktJPA.setPrice(getInput(doubleChoice));
        
        out.println("Client: ");
        stringChoice = input.next();
        tkt.setClient(getInput(stringChoice));
        tktJPA.setClient(getInput(stringChoice));
        
        tkt.sellCopy(indexChoice);
          
        // Put in the pubList list array. 
        ticketList.add(tkt);
        ct.sellItem(tkt);
        
        
        // create EMF with persistent unit as parameter
        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        
        TicketJpaController ticketJpaController = new TicketJpaController(emf);
        
 
        ticketJpaController.create(tktJPA);
    }
    
    private void sellPencil() {
        // Holds user input
        String stringChoice;
        double doubleChoice;
        int indexChoice = 0;
        
        Pencil pencil = new Pencil();
        PencilJPA pencilJPA = new PencilJPA();

        out.println("Color: ");
        stringChoice = input.next();
        pencil.setColor(getInput(stringChoice));
        pencilJPA.setColor(getInput(stringChoice));

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        pencil.setPrice(getInput(doubleChoice));
        pencilJPA.setPrice(getInput(doubleChoice));

        out.println("Client: ");
        stringChoice = input.next();
        pencil.setClient(getInput(stringChoice));
        pencilJPA.setClient(getInput(stringChoice));

        ct.sellItem(pencil);
        pencil.sellCopy(indexChoice);
        
        // create EMF with persistent unit as parameter
        emf=Persistence.createEntityManagerFactory("w21_lab5_PU");
        
        PencilJpaController pencilJpaController = new PencilJpaController(emf);
        
 
        pencilJpaController.create(pencilJPA);
    }
    
    
    
    private void runningTotal() {
        out.println(ct.showTotal());
    }
    
}
