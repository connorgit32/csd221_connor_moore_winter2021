/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import lab5.entities.SaleableItem;



/**
 *
 * @author connor
 */
public class DiskMagazine extends Magazine implements SaleableItem {
    
    private int orderQty;
    private int currIssue;
    private boolean hasDisk;
    
    /**
     * @return the orderQty
     */
    public int getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the currIssue
     */
    public int getCurrIssue() {
        return currIssue;
    }

    /**
     * @param currIssue the currIssue to set
     */
    public void setCurrIssue(int currIssue) {
        this.currIssue = currIssue;
    }

    /**
     * @return the hasDisk
     */
    public boolean isHasDisk() {
        return hasDisk;
    }
    
    
    
    
    
}
