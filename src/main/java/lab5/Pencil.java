/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import lab5.entities.SaleableItem;



/**
 *
 * @author connor
 */
public class Pencil extends Publication implements SaleableItem {
    
    private String color;
    private double price;
    private String client;
    
    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }   
    
    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }
    
    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }
    
    public void sellCopy(int indexChoice) 
    {
        System.out.println("Pencil Sold!"); 
    }
    
    
    
   
    @Override
    public String toString() {
        return    "Client: " + getClient() + "\n"
                + "Color: " + getColor() + "\n"
                + "Price: " + getPrice() + "\n";      
    }
}
