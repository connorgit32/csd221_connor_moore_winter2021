package Connor_Moore_test4_practical.domain;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author connor
 */

@Entity
public class ShapeInterface_Moore {

    @Id
    @GeneratedValue
    private Long id;
    @Basic
    private String printArea;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrintArea() {
        return printArea;
    }

    public void setPrintArea(String printArea) {
        this.printArea = printArea;
    }

}