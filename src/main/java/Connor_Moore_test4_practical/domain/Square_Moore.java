package Connor_Moore_test4_practical.domain;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author connor
 */

@Entity
public class Square_Moore extends Shape_Moore {

    @Basic
    private String theLength;
    @Basic
    private String width;

    public String getTheLength() {
        return theLength;
    }

    public void setTheLength(String theLength) {
        this.theLength = theLength;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

}