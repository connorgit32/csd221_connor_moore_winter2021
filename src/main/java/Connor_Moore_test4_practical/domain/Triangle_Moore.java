package Connor_Moore_test4_practical.domain;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author connor
 */

@Entity
public class Triangle_Moore extends Shape_Moore {

    @Basic
    private String base;
    @Basic
    private String height;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

}