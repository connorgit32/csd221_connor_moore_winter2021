/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Connor_Moore_test4_practical.Controller;

import Connor_Moore_test4_practical.*;
import Connor_Moore_test4_practical.domain.Square_Moore;
import Connor_Moore_test4_practical.domain.Triangle_Moore;

/**
 *
 * @author connor
 */
public class AppWithController {

    void run() {
        
        Square_Moore s1 = new Square_Moore();
        Square_MooreJpaController sc1 = new Square_MooreJpaController();
        
        Triangle_Moore t1 = new Triangle_Moore();
        Triangle_MooreJpaController tc1 = new Triangle_MooreJpaController();
        
        sc1.create(s1);
        tc1.create(t1);
        
        
    }
   
    
}
