/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Connor_Moore_test4_practical.Controller;

import Connor_Moore_test4_practical.domain.Triangle_Moore;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import lab5.controllers.exceptions.NonexistentEntityException;

/**
 *
 * @author connor
 */
public class Triangle_MooreJpaController {
    public Triangle_MooreJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    Triangle_MooreJpaController() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Triangle_Moore Triangle_Moore) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(Triangle_Moore);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public void edit(Triangle_Moore Triangle_Moore) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Triangle_Moore = em.merge(Triangle_Moore);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = Triangle_Moore.getId();
                if (Square_Moore(id) == null) {
                    throw new NonexistentEntityException("The bookJPA with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Triangle_Moore Triangle_Moore;
            try {
                Triangle_Moore = em.getReference(Triangle_Moore.class, id);
                Triangle_Moore.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bookJPA with id " + id + " no longer exists.", enfe);
            }
            em.remove(Triangle_Moore);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private Object Square_Moore(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
