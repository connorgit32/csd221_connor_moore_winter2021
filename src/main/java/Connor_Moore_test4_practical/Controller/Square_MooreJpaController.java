/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Connor_Moore_test4_practical.Controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import Connor_Moore_test4_practical.domain.Square_Moore;
import javax.persistence.EntityNotFoundException;
import lab5.controllers.exceptions.NonexistentEntityException;

/**
 *
 * @author connor
 */
public class Square_MooreJpaController {
    public Square_MooreJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    Square_MooreJpaController() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Square_Moore Square_Moore) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(Square_Moore);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public void edit(Square_Moore Square_Moore) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Square_Moore = em.merge(Square_Moore);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = Square_Moore.getId();
                if (Square_Moore(id) == null) {
                    throw new NonexistentEntityException("The bookJPA with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Square_Moore Square_Moore;
            try {
                Square_Moore = em.getReference(Square_Moore.class, id);
                Square_Moore.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bookJPA with id " + id + " no longer exists.", enfe);
            }
            em.remove(Square_Moore);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private Object Square_Moore(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
