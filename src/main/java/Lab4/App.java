/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

import java.util.*;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author connor
 */
public class App {

    // If true, all code within run() will loop; if false end the program.
    public boolean loopBool = true;

    // Creates an object called 'input' from the class 'Scanner' which is a
    // part of the java.util library.
    final Scanner input = new Scanner(System.in);

    // List for each type of publication
    static List<Publication> pubList = new ArrayList<Publication>();
    
    // CashTill object that stores a running total
    CashTill ct = new CashTill();
    
    // List for each ticket
    static List<Ticket> ticketList = new ArrayList<Ticket>();
    
     // A menu to be displayed to the user.
    final String menu = ""
            + "1. Add Book\n"
            + "2. Add Magazine\n"
            + "3. Add Disk Magazine\n"
            + "4. Edit Publication\n"
            + "5. List Publication\n"
            + "6. Delete Publication\n"
            + "7. Sell Publication\n"
            + "8. Sell Ticket\n"
            + "9. Sell Pencil\n"
            + "10. Running total\n"
            + "99. quit";

    public void run() {

        while (loopBool == true) { // allows program to run in a loop, inputting 99 exits

            out.println(menu);

            int choice = 0;

            try { // This try-catch re-prompts the user for a menu selection if a non-integer is input.
                choice = input.nextInt();
            } catch (Exception e) {
                System.out.println("Please verify your selection was correct.");
                continue;
            }

            switch (choice) {
                case 1:
                    addBook();
                    break;
                case 2:
                    addMag();
                    break;
                case 3:
                    addDiskMag();
                    break;
                case 4:
                    editPub();
                    break;
                case 5:
                    listPub();
                    break;
                case 6:
                    deletePub();
                    break;
                case 7:
                    sellPub();
                    break;
                case 8:
                    sellTicket();
                    break;
                case 9:
                    sellPencil();
                    break;
                case 10:
                    runningTotal();
                    break;
                case 99:
                    loopBool = false;
                    break;
                default:
                    System.out.println("Bad input, try again please.");
            }
        }
    }
    
   
    public void addBook() {

        // Holds user input
        String stringChoice;
        int intChoice;
        double doubleChoice;

        Book book = new Book();

        // Inserts all needed attributes of Publication and Book objects.
        out.println("Title: ");
        stringChoice = input.next();
        book.setTitle(stringChoice);

        out.println("Copies: ");
        intChoice = input.nextInt();
        book.setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        book.setPrice(doubleChoice);
        
        out.println("Author: ");
        stringChoice = input.next();
        book.setAuthor(stringChoice);

        // Put in the pubList list array. 
        pubList.add(book);

    }
    
    public void addMag() {

        // Holds user input
        String stringChoice;
        int intChoice;
        double doubleChoice;

        Magazine mag = new Magazine();

        // Inserts all needed attributes of Publication and Magazine objects.
        out.println("Title: ");
        stringChoice = input.next();
        mag.setTitle(stringChoice);
        
        out.println("Copies: ");
        intChoice = input.nextInt();
        mag.setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        mag.setPrice(doubleChoice);

        out.println("Order Quantity: ");
        intChoice = input.nextInt();
        mag.setOrderQty(intChoice);
        
        out.println("Current Issue: ");
        intChoice = input.nextInt();
        mag.setCurrIssue(intChoice);
        
        // Put in the pubList list array. 
        pubList.add(mag);

    }

    
    public void addDiskMag() {

        // Holds user input
        String stringChoice;
        int intChoice;
        double doubleChoice;

        DiskMagazine mag = new DiskMagazine();

        // Inserts all needed attributes of Publication and Magazine objects.
        out.println("Title: ");
        stringChoice = input.next();
        mag.setTitle(stringChoice);

        out.println("Copies: ");
        intChoice = input.nextInt();
        mag.setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        mag.setPrice(doubleChoice);

        out.println("Order Quantity: ");
        intChoice = input.nextInt();
        mag.setOrderQty(intChoice);
        
        out.println("Current Issue: ");
        intChoice = input.nextInt();
        mag.setCurrIssue(intChoice);
        
        // Put in the pubList list array. 
        pubList.add(mag);

    }
    
    
    
    
    
    
    private void editPub() {

        int indexChoice; // stores pubList index
        int intChoice;
        String stringChoice;
        double doubleChoice;

        out.println("Which publication number would you like to edit?: ");
        indexChoice = input.nextInt();

        // Inserts all needed attributes of Publication object.
        out.println("Title: ");
        stringChoice = input.next();
        pubList.get(indexChoice).setTitle(stringChoice);

        out.println("Copies: ");
        intChoice = input.nextInt();
        pubList.get(indexChoice).setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        pubList.get(indexChoice).setPrice(doubleChoice);

    }

    private static void listPub() {
        // Lists all objects in list array using a for loop and the get() list array 
        // library method which uses indexes 
        for (int i = 1; i <= pubList.size(); i++) { // i = 1, and <= to account for array syntax
            System.out.println("Publication #" + i);      // making it more readable. First in array 
            System.out.println(pubList.get(i - 1)); // will be object 1, not object 0.
        }
        // .get works by utilizing the toString()
        // It can be overridden using @Override to customize what is displayed.
    }

    
    private void deletePub() {

        int indexChoice; // choose a person from the personList list array

        out.println("Which publication number would you like to delete?: ");
        indexChoice = input.nextInt();

        // Uses the Array List method remove() to delete people which handles
        // null spaces inbetween objects
        pubList.remove(indexChoice - 1); // - 1 because we want people in the list
        // to start at 1, not 0 which is default for arrays.

    }

    private void sellPub() {
        int indexChoice; // choose a person from the personList list array

        out.println("Which publication number would you like to sell?: ");
        indexChoice = input.nextInt();
                       
        
        ct.sellItem(pubList.get(indexChoice));
        
        
        // Lab 4 Didn't have -1 here, so sellPub() did not work.
        pubList.remove(indexChoice - 1);
    }
    
    
    private void sellTicket() {
        // Holds user input
        String stringChoice;
        double doubleChoice;
        int indexChoice = 0;
        
        Ticket tkt = new Ticket();

        // Inserts all needed attributes of Publication and Magazine objects.
        out.println("Description: ");
        stringChoice = input.next();
        tkt.setDescription(stringChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        tkt.setPrice(doubleChoice);

        out.println("Client: ");
        stringChoice = input.next();
        tkt.setClient(stringChoice);

        
        tkt.sellCopy(indexChoice);
          
        // Put in the pubList list array. 
        ticketList.add(tkt);
        ct.sellItem(tkt);
    }
    
    private void sellPencil() {
        // Holds user input
        String stringChoice;
        double doubleChoice;
        int indexChoice = 0;
        
        Pencil pencil = new Pencil();

        out.println("Color: ");
        stringChoice = input.next();
        pencil.setColor(stringChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        pencil.setPrice(doubleChoice);

        out.println("Client: ");
        stringChoice = input.next();
        pencil.setClient(stringChoice);

        ct.sellItem(pencil);
        pencil.sellCopy(indexChoice);
        

    }
    
    
    
    private void runningTotal() {
        out.println(ct.showTotal());
    }
    
}
