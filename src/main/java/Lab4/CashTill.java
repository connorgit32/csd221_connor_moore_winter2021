/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

/**
 *
 * @author connor
 */
public class CashTill {
    
    private double runningTotal;
    CashTill()
    {
        runningTotal = 0;
    }
    
    public void sellItem (Publication pub) 
    {
        runningTotal += pub.getPrice();
        //pub.sellCopy(indexChoice);
    }
    
    public double showTotal() 
    {
        return runningTotal;
    }
}
