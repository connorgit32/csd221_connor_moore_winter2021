/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;


/**
 *
 * @author connor
 */
public class Magazine extends Publication {
    
    private int orderQty;
    private int currIssue;

    /**
     * @return the orderQty
     */
    public int getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the currIssue
     */
    public int getCurrIssue() {
        return currIssue;
    }

    /**
     * @param currIssue the currIssue to set
     */
    public void setCurrIssue(int currIssue) {
        this.currIssue = currIssue;
    }

    public void sellCopy(int indexChoice) {

        App.pubList.remove(indexChoice - 1);
        
    }
    
    
    
    
    
}
