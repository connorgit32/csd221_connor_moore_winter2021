/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1.q6;

/**
 *
 * @author connor
 */
public class Person {
    
    private String firstName;
    private String lastName;
    private int SIN;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the SIN
     */
    public int getSIN() {
        return SIN;
    }

    /**
     * @param SIN the SIN to set
     */
    public void setSIN(int SIN) {
        this.SIN = SIN;
    }
    
    
    @Override
    public String toString() {
        
        return "First Name: " + firstName + "\n"
                + "Last Name: " + lastName + "\n"
                + "SIN: " + SIN + "\n";
                //+ "First Name : " + p.getFirstName() + "\n"
                //+ "Last Name : " + p.getLastName();       
    }
    
}
