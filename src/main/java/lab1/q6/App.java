/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lab1.q6;
import java.util.*;
import static java.lang.System.out;

/**
 *
 * @author connor
 */
public class App {
    
    // If true, all code within run() will loop; if false end the program.
    public boolean loopBool = true; 
                                    
    // Creates an object called 'input' from the class 'Scanner' which is a
    // part of the java.util library.
    final Scanner input = new Scanner(System.in); 
    
    // Creates an array list 'personList' that accepts 'Person' objects.
    // Array lists allow us to use .add(Person) and .remove(Person) to
    // add and remove objects handling the shuffling that must be done
    // when an object wedged between two other objects is removed. This
    // must be done manually with normal arrays, else you have NULL values.
    static List<Person> personList = new ArrayList<Person>(); //
    
    final String menu = "" // A menu to be displayed to the user.
        + "1. Add Person\n"
        + "2. List Person\n"
        + "3. Delete Person\n"  
        + "99. quit";
    
    
    public void run() {
  

        while (loopBool == true) { // allows program to run in a loop, inputting 99 exits
                
                out.println(menu); 
            
                int choice=0;
                
                try{ // This try-catch re-prompts the user for a menu selection if a non-integer is input.
                    choice=input.nextInt();
                }catch(Exception e){
                    System.out.println("Please verify your selection was correct.");
                    continue;
                }    

                switch(choice){
                    case 1: addPerson();
                            break;
                    case 2: listPeople();
                            break;
                    case 3: deletePerson();
                            break;
                    case 99: loopBool = false;
                            break;
                    default: System.out.println("Bad input, try again please.");
                }
        }
    }

    
    public void addPerson() {
        
        // Holds user input for strings (First/Last names) and an int (SIN).
        String stringChoice;
        int intChoice;
        
        Person person = new Person(); // Create new person object every time addPerson() is called.
        
        // Inserts all needed attributes of person object.
        out.println("First Name: "); 
        stringChoice = input.next();
        person.setFirstName(stringChoice); 
      
        out.println("Last Name: ");
        stringChoice = input.next();
        person.setLastName(stringChoice); 
             
        out.println("SIN: ");
        intChoice = input.nextInt();
        person.setSIN(intChoice); 
        
        // Car object which now has data stored in it, is put in the personList list array. 
        personList.add(person);
               
    }   
    
    
    private void listPeople() {
        // Lists all people is personList list array using a for loop and the get() list array 
        // library method which uses indexes 
        for (int i = 1; i <= personList.size(); i++) { // i = 1, and <= to account for array syntax
            System.out.println("Person #"+i+": ");      // making it more readable. First person in array 
            System.out.println(personList.get(i - 1)); // will be person 1, not person 0.
        }
        // .get works by utilizing the toString() default method found in the 'Person' class.
        // It can be overridden using @Override to customize what is displayed.
    }
    
    
    private void deletePerson() {
        
        int indexChoice; // choose a person from the personList list array
        
        out.println("Which person number would you like to delete?: ");
        indexChoice = input.nextInt();
        
        // Uses the Array List method remove() to delete people which handles
        // null spaces inbetween objects
        personList.remove(indexChoice - 1); // - 1 because we want people in the list
                                            // to start at 1, not 0 which is default for arrays.

    }
    
    
}