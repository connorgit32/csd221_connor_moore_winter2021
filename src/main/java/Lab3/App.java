/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3;

import java.util.*;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author connor
 */
public class App {

    // If true, all code within run() will loop; if false end the program.
    public boolean loopBool = true;

    // Creates an object called 'input' from the class 'Scanner' which is a
    // part of the java.util library.
    final Scanner input = new Scanner(System.in);

    // List for each type of publication
    static List<Publication> pubList = new ArrayList<Publication>();
    

    final String menu = "" // A menu to be displayed to the user.
            + "1. Add Publication\n"
            + "2. Add Book\n"
            + "3. Add Magazine\n"
            + "4. Edit Publication\n"
            + "5. List Publication\n"
            + "6. Delete Publication\n"
            + "99. quit";

    public void run() {

        while (loopBool == true) { // allows program to run in a loop, inputting 99 exits

            out.println(menu);

            int choice = 0;

            try { // This try-catch re-prompts the user for a menu selection if a non-integer is input.
                choice = input.nextInt();
            } catch (Exception e) {
                System.out.println("Please verify your selection was correct.");
                continue;
            }

            switch (choice) {
                case 1:
                    addPub();
                    break;
                case 2:
                    addBook();
                    break;
                case 3:
                    addMag();
                    break;
                case 4:
                    editPub();
                    break;
                case 5:
                    listPub();
                    break;
                case 6:
                    deletePub();
                    break;
                case 99:
                    loopBool = false;
                    break;
                default:
                    System.out.println("Bad input, try again please.");
            }
        }
    }
    
    public void addPub() {

        // Holds user input
        String stringChoice;
        int intChoice;
        double doubleChoice;

        Publication publication = new Publication(); // Create new Publication object every time method called

        // Inserts all needed attributes of Publication object.
        out.println("Title: ");
        stringChoice = input.next();
        publication.setTitle(stringChoice);

        out.println("Copies: ");
        intChoice = input.nextInt();
        publication.setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        publication.setPrice(doubleChoice);

        // Put in the pubList list array. 
        pubList.add(publication);

    }

    public void addBook() {

        // Holds user input
        String stringChoice;
        int intChoice;
        double doubleChoice;

        Book book = new Book();

        // Inserts all needed attributes of Publication object.
        out.println("Title: ");
        stringChoice = input.next();
        book.setTitle(stringChoice);

        out.println("Copies: ");
        intChoice = input.nextInt();
        book.setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        book.setPrice(doubleChoice);
        
        out.println("Author: ");
        stringChoice = input.next();
        book.setAuthor(stringChoice);

        // Put in the pubList list array. 
        pubList.add(book);

    }
    
    public void addMag() {

        // Holds user input
        String stringChoice;
        int intChoice;
        double doubleChoice;

        Magazine mag = new Magazine();

        // Inserts all needed attributes of Publication object.
        out.println("Title: ");
        stringChoice = input.next();
        mag.setTitle(stringChoice);

        out.println("Copies: ");
        intChoice = input.nextInt();
        mag.setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        mag.setPrice(doubleChoice);

        out.println("Order Quantity: ");
        intChoice = input.nextInt();
        mag.setOrderQty(intChoice);
        
        out.println("Current Issue: ");
        intChoice = input.nextInt();
        mag.setCurrIssue(intChoice);
        
        // Put in the pubList list array. 
        pubList.add(mag);

    }

    private void editPub() {

        int indexChoice; // stores pubList index
        int intChoice;
        String stringChoice;
        double doubleChoice;

        out.println("Which publication number would you like to edit?: ");
        indexChoice = input.nextInt();

        // Inserts all needed attributes of Publication object.
        out.println("Title: ");
        stringChoice = input.next();
        pubList.get(indexChoice).setTitle(stringChoice);

        out.println("Copies: ");
        intChoice = input.nextInt();
        pubList.get(indexChoice).setCopies(intChoice);

        out.println("Price: ");
        doubleChoice = input.nextDouble();
        pubList.get(indexChoice).setPrice(doubleChoice);

    }

    private static void listPub() {
        // Lists all objects in list array using a for loop and the get() list array 
        // library method which uses indexes 
        for (int i = 1; i <= pubList.size(); i++) { // i = 1, and <= to account for array syntax
            System.out.println("Publication #" + i);      // making it more readable. First in array 
            System.out.println(pubList.get(i - 1)); // will be object 1, not object 0.
        }
        // .get works by utilizing the toString()
        // It can be overridden using @Override to customize what is displayed.
    }

    
    private void deletePub() {

        int indexChoice; // choose a person from the personList list array

        out.println("Which publication number would you like to delete?: ");
        indexChoice = input.nextInt();

        // Uses the Array List method remove() to delete people which handles
        // null spaces inbetween objects
        pubList.remove(indexChoice - 1); // - 1 because we want people in the list
        // to start at 1, not 0 which is default for arrays.

    }

}
