/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3;


/**
 *
 * @author connor
 */
public class Book extends Publication {
    
    private String author;
    
    
    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }
    
    public String toString() {
        
        return "Author: " + author + "\n";
                                                           
    }

    
}
